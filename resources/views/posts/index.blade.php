@extends('layouts.app')
@section('content')
    <h1>Posts</h1>
    @if(count($posts) > 0)
        @foreach($posts as $post)
                <div class="card mb-5 mx-auto" style="width: 50rem;">
                        <img class="card-img-top" src="/storage/cover_images/{{$post->cover_image}}" style="width:100%;" alt="">
                        <div class="card-body">
                            <h3 class="card-title">{{$post->title}}</h3>
                            <p class="card-text">Written on {{$post->created_at}} by {{$post->user->name}}</p>
                            <a href="/posts/{{$post->id}}" class="btn btn-primary">View more</a>
                        </div>
                </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection